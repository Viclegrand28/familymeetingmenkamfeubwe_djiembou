# FAMILLE MENKAM FEUBWE (BRANCHE DJIEMBOU)
![DJIEMBOU NICODEME, Header of Family](./papa.jpg)
## REUNION FAMILIALE 2021

> #### SOUS LA 
>> - CHEF DE FAMILLE: MADAME DJIEMBOU MONGWE PAULINE
>> - SECRETAIRE: DJIEMBOU TIENTCHEU VICTOR NICO
>> - MEMBRE: DJIEMBOU PADJE THERESE ARMANCE, DJIEMBOU YOSSA CHRISTIANE L'OR, DEUMENI MARIE GAELLE, DJIEMBOU PRINCE ALFRED, TCHAMGOUE FOMENI BRYAN STYVE, DJIEMBOU NGADEU MARIE GISELLE.

### ORDRE DU JOUR

>REUNION PORTANT
>> 1. PRIERE D'OUVERTURE
>> 2. NOUVELLES DE L'ANNEE
>> 3. COMMENT GERER LA FAMILLE DANS LE BUT DE REUSSIR ENSEMBLE?
>> 4. LES PREVISIONS POUR LE PROBLEME DE LA MAISON DE PA'A CHI ET DE PA'A
>> 5. COMMENT FAIRE POUR METTRE UNE BARRIERE POUR LA MAISON DE DOUALA?
>> 6. REMERCIEMENT POUR LA VENU D'UN NOUVEAU MEMBRE A LA FAMILLE DJIEMBOU (DE GISELLE)
>> 7. LES CONSEILLES DE MAMAN
>> 8. RELANCEMENT DE LA -TONTINE DES ENFANTS DE LA FAMILLE DJIEMBOU-
>> 9. COLLATION ET DIVERS

### REALISATION DE LA REUNION

> #### DEBUT DE LA REUNION --08:10 PM--
>> - PRIERE EFFECTUE PAR *MAMAN*
>> - NOUVELLES DE L'ANNEE
>>>	MAMAN
>>>> LA VENUE DE NEVEUX ET NIECES DANS LA FAMILLE (ARMANCE, MARTIN, GISELLE), 
>>>> CAS DE MALADIE INCESSANTE NOTAMMENT LES PROBLEMES DE VUE.
>>>	ARMANCE
>>>> RENOUVELLEMENT DE CONTRAT DE TRAVAIL.
>>>	GAELLE
>>>> EVOLUTION DANS SON TRAVAIL DE COUTURE A TRAVERS L'ACQUISITION D'OUTILS DE TRAVAIL.
>>>	GISELLE
>>>> REMERCIEMENT POUR LA PRISE EN MAIN DE LA FAMILLE PAR LE SEIGNEUR, LES MALADIES ON BEAUCOUP DERANGEES LA FAMILLE, EVOLUTION DE LA MAISON DE DOUALA, 
>>>	VICTOR
>>>> L'ACHEVEMENT DES ETUDES DE FORMATION, DEBUT D'AUTONOMIE AVEC SON FREELANCE.
>>>	CHRISTIANE
>>>> SANTE PLUS SUR POUR CETTE ANNEE, CONSTANCE EXISTANCIEL.
>>>	BRYAN
>>>> AGREABLE ANNEE.
>>>	PRINCE
>>>> AGREABLE ANNEE, CRAINTE DU COVID-19 ET CONSEQUENCE DESASTREUSES.
>> -	COMMENT GERER LA FAMILLE DANS LE BUT DE REUSSIR ENSEMBLE?
>>>	ARMANCE
>>>> EVITER LA CONCURRENCE, PRONER LA SOLIDARITE, UNION = FAMILLE. 
>>>	GAELLE
>>>> IDEM, PENSER AUX AUTRES, PENSER AVANT D'AGIR ET AGIR DANS LE BUT BIEN FAIRE EN FAVEUR DU SEIGNEUR ET PUIS EN FAVEUR DE SES PROCHES, ADMETTRE LES CONSEILLES VENANT DE TOUT LE MONDE DANS LA FAMILLE.
>>>	VICTOR
>>>> EVITER D'ECOUTER LES INFORMATIONS ET S'EN TENIR, CE SERAI MIEUX DE QUESTIONNAIRE ET ETRE DIRECTE SUR LA REPONSE, SI ELLE N'EST PAS SATISFAISANTE, DIRE SANS CRAINTE; APPRENDRE A DEMANDER CONSEILLE.
>>>	CHRISTIANE
>>>> S'ENTRE-AIDER, FAIRE EVOLUER LES MEMBRES DE LA FAMILLE DE FACON EQUITABLE AFIN D'ALLERGER LA TACHE SUR LA FAMILLE LORS DE PLANIFICATION D'EVENEMENT, SAVOIR ECOUTER ET GARDER CALME.
>>>	BRYAN
>>>> NE PAS MENTIR, RESPECT DU PROCHAIN.
>>>	GISELLE
>>>> 
>>>	PRINCE
>>>> NE RIEN CACHER A LA FAMILLE, ETRE UMBLE, OBEISSANCE, RESPECT DES PROCHAINS, LE PARDON, COMPATION, AMOUR FRATERNELLE ET MATERNELLE.
>>>	MAMAN
>>>> VALIDATION DES DIRES PRECEDENTES
>> - LES PREVISIONS POUR LE PROBLEME DE LA MAISON DE PA'A CHI ET DE PA'A
>>>	ARMANCE
>>>> CHERCHER UN TECHNICIEN POUR FAIRE UN DEVIS AFIN DE TROUVER UN MOYEN DE FAIRE AU MOINS DE FAIRE 2 OU 3 CHAMBRE AFIN DE VOIRE COMMENT DE CONSTRUIRE CELA, L'INTERET DEVRAIT PLUS PORTER PAR LES ENFANTS GARCONS DE LA FAMILLE.
>>>	GAELLE
>>>> IDEM
>>>	VICTOR
>>>> IDEM
>>>	CHRISTIANE
>>>> IDEM
>>>	MAMAN
>>>> IDEM
>> - COMMENT FAIRE POUR METTRE UNE BARRIERE POUR LA MAISON DE DOUALA?
>>>	ARMANCE
>>>> UN DEVIS DE 350000 AVAIS DEJA ETE DONNEE
>>>	GAELLE
>>>> 
>>>	VICTOR
>>>> 
>>>	CHRISTIANE
>>>> 
>>>	MAMAN
>>>> 
>> - REMERCIEMENT POUR LA VENU D'UN NOUVEAU MEMBRE A LA FAMILLE DJIEMBOU (DE GISELLE)
>> - LES CONSEILLES DE MAMAN
>>> CI NOUS SOMMES CHEZ LES GENS, VIVONS CHEZ LES GENS, NE PAS PRENDRE LES HABITUDES DES AUTRES POUR PARAITRE. ETRE VIGILANT DANS LA VIE, SAVOIR AVEC QUI NOUS MARCHONS.
>> - RELANCEMENT DE LA TONTINE DES ENFANTS DE LA FAMILLE DJIEMBOU
>>> PROMPTITUDE DE VERSEMENT, DEFINITION DE PENALITE, REGLE DE GESTION REGISANT LA TONTINE.
>> - COLLATION ET DIVERS
>>> 

> #### TONTINES
>> - MONTANT FIXÉ: 10.000 XAF
>> - DATE DE PERCEPTION DES TONTINES : 15 OF MONTH'S
>> - MONTANT A PERCEVOIR : 50.000 XAF
>> - REGLE DE TONTINE
>>> 1. AUCUN RETARD N'EST PERMIS SANS EXCUSE VALABLE
>>> 2. POUR UN RETARD LA SANCTION EST DE 5.000 XAF
>> - ORDRE DE PASSAGE
>>> 1. GAELLE √√ net percu : 50 000 XAF au mois d'avril 2021
>>> 2. GISELLE 
>>> 3. MATHIEU
>>> 6. MAMAN
>>> 7. CHRISTIANE
>>> 9. VICTOR

> #### BARRIERE DE DOUALA
>> REPARTITION DES MONTANTS
>>> - SAMUEL : 65.000 XAF
>>> - ARMANCE : 65.000 XAF
>>> - CHRISTIANE : 48.000 XAF
>>> - MARTIN : 48.000 XAF
>>> - GAELLE : 35.000 XAF
>>> - MATHIEU : 48.000 XAF
>>> - GISELLE : 35.000 XAF
>>> - VICTOR : 20.000 XAF
>>
>> DELAIS : 6 MOIS ACCORDÉS A PARTIR DE MOIS D'AVRIL

## REUNION FAMILIALE 2022

> #### SOUS LA
>>
>>  - **CHEF DE FAMILLE**: MADAME DJIEMBOU MONGWE PAULINE
>>  - **SECRETAIRE**: DJIEMBOU TIENTCHEU VICTOR NICO
>>  - **MEMBRE**:  
>>>     + SIEWE SAMUEL NICODEME,
>>>     + DJIEMBOU PADJE THERESE ARMANCE, 
>>>     + DEUMENI MARIE GAELLE, 
>>>     + DJIEMBOU YONGOUE MATHIEU HERVE,
>>>     + DJIEMBOU NGADEU MARIE GISELLE,
>>>     + DJIEMBOU PRINCE ALFRED,
>>>     + TCHAMGOUE FOMENI BRYAN STYVE. 

### ORDRE DU JOUR

> **REUNION PORTANT**
>>
>>  1. NOUVELLES DE L'ANNEE DES MEMBRES
>>  2. PRIERE D'OUVERTURE
>>  3. RAPPEL DE LA DERNIÈRE SÉANCE
>>  4. PROBLÈMES LATENTS DE LA FAMILLE ET REMÉDIATIONS
>>>     A) LES PREVISIONS POUR LE PROBLEME DE LA MAISON DE PA'A CHI ET DE PA'A
>>>     B) COMMENT FAIRE POUR METTRE UNE BARRIERE POUR LA MAISON DE DOUALA?
>>>     C) PROBLÈME D'OBTENTION DU TITRE FONCIER DE LA MAISON DE DOUALA
>>>     D) LES CONSEILLES DE MAMAN
>>>     E) DEVIS ESTIMATIF POUR LES TOILETTES DE LA MAISON DE DOUALA
>>>     F) AUTRES.
>>  5. RELANCEMENT DE LA TONTINE DES ENFANTS DE LA FAMILLE **DJIEMBOU**
>>  6. COLLATION ET DIVERS

### REALISATION DE LA REUNION

> #### DEBUT DE LA REUNION --11:47 PM--
>>
>> **1. NOUVELLES DE L'ANNEE DES MEMBRES**
>>>
>>> **Brayan**: dit que son année a été plutot bien, la chose la plus bien pour lui à été le qu'il soit sorti parmi les 10 prémiers et sa difficulté a été le fait de ne pas être parmi les 5 premiers.
>>> **Prince**: dit que de son coté rien ne particulier, juste une fierté de sa part nous voire tous reuni.
>>> **Victor**: de son coté, il fait remarqué la présence d'une forte période de mauvaise santé, mais qui a permis de faire resortir les choses bien caché mais ce qui n'est pas plus mal. Début de job dans une startup de la place.
>>> **Gisele**: Anniversaire de papa Sam. juste pleine de difficulté familiales, notamment la situation de tout un chacun dans la famille qui n'a pas vraiment changé. Les situation difficile avec l'accident du marie de Armance qui a beaucoup affecté la majorité d'entre nous, la situation d'état divorce de notre grand frère ainé qui nous aussi beaucoup mis à terre.
>>> **Mathieu**: fierté de vraiment nous revoire tous reuni ici, pleine de chose à dire mais il s'appluit plus sur le point de la nécéssité de venir à la maison avec une chèvre pour nous tous.
>>> **Gaelle**: sur tout ce qui deja été mentionné plus haut, elle fait aujourd'hui une étude en couture.
>>> **Armance**: situation compliqué avec la ménagéaire, qui suit à de nombreux situations maladies et à des crises d'épilepsie(selon maman de la ménagéaire, dis une attaque spirituelle). Comme bonne nouvelle, il y'a une très grande envie de la part des enfants d'apprendre la langue NUFI.
>>> **Samuel**: Dieu permet à ce que nous puissions toujours nous relever de tous nos problème, la procedure judiciere concernant le divorce avec helène à été pour une partie dejà coupé mais il y'a une autre partie qui va l'être le 13 Avril 2022. Concernant les repartitions, rien n'a été dit. Coté santé, juste une douleur au pieds qui de temps à autre le nuit. La conséquente d'une action passée qui continu à miner sa vie mais tout est remis dans la grâce de Dieu.
>>> **Maman**: 
>>>
>> **2. PRIERE D'OUVERTURE (Mathieu)**
>>> 
>> **3. RAPPEL DE LA DERNIÈRE SÉANCE**
>>> 
>> **4. PROBLÈMES LATENTS DE LA FAMILLE ET REMÉDIATIONS**
>>>
>>>     A) LES PREVISIONS POUR LE PROBLEME DE LA MAISON DE PA'A CHI ET DE PA'A
>>>     B) COMMENT FAIRE POUR METTRE UNE BARRIERE POUR LA MAISON DE DOUALA?
>>>>
>>>>    Gaelle a déjà donnée 10.000 XAF chez Christian, donc elle devient automatiquement la caissière pour ce chemin.
>>>>    Il est a noter que nous posons comme date de prévision pour la fin du mois.
>>>>	Résumé: quête de 10.000 XAF au près de chacun pour cet effet
>>>>	Aussi exigences pour les enfants Djiembou de cotiser chaque moi maman d'un montant de 5.000 XAF pout l'aider avec le gestion d'eau et d'énergie.
>>>> 
>>>     C) PROBLÈME D'OBTENTION DU TITRE FONCIER DE LA MAISON DE DOUALA
>>>>
>>>>    Nous sommes déjà en possession du documents requis, il ne manque plus que le montant requis qui s'élève à au moins 170.000 XAF.
>>>>
>>>     D) LES CONSEILLES DE MAMAN
>>>>    Ici les remarquons les desir de maman de toujours être electroniquement financer pour gérer les soucis d'électricité, eau et autres. Donc la resolution, est donc de générer une habitudes de toujours faire une signe financier à la maman tous les mois et que cette habitudes soit évalué à chaque séances.
>>>     E) DEVIS ESTIMATIF POUR LES TOILETTES DE LA MAISON DE DOUALA
>>>>
>>>>    careau de sol et de mur, ustensil de lavabo, blombier, ...
>>>>    Il y'a une estimation totale de 300.000 XAF.
>>>> 
>>>     F) AUTRES.
>>>>
>>>>    Mathieu, Armance, Samuel(tous les enfants de la famille) doivent être trouver un moyen de se stabiliser(à l'exemple de contruire une maison) qui permettre de mieux traiter les problèmes familiaux. 
>>>>    L'object à court terme est que nous ainés puisse avant 2023 avoir fini avec leur projet de construction de domicile.
>>>
>>> **5. RELANCEMENT DE LA TONTINE DES ENFANTS DE LA FAMILLE** _DJIEMBOU_
>>>>
>>>>    MATHIEU, ARMANCE, GISELE, GAELLE, CHRISTIANE, MARTIN : 25.000 XAF
>>>>    VICTOR, MAMAN : 15.000 XAF
>>>>    **ORDRE**
>>>>    **Date limite: 30 de chaque mois**
>>>>    1- MATHIEU -> AVRIL
>>>>    2- GISELE -> MAY
>>>>    3- GAELLE -> JUNE
>>>>    4- CHRISTIANE -> JULY
>>>>    5- MAMAN -> AUGUST
>>>>    6- VICTOR -> SEPTEMBER
>>>>    7- MARTIN -> OCTOBER
>>>>    7- ARMANCE -> NOVEMBER
>>>>    **TOTAL : 25.OOO XAF * 6 + 15.000 XAF*2 => 180.000 XAF**
>>>>    **VICTOR EST LE TRESORIER DE CETTE REUNION, CHAQUE TRANSFERT DEVRA INCOMPORER LES FRAIS DE TRANSFERT**
>>>>    **PENALITÉ ECHEC TONTINE**
>>>>    **SI TU AS DEJA BOUFFÉ, LA PÉNALITÉ EST ÉGALE LA MOTIÉ DE TA COTISATION**
>>>>    **SI TU N'AS PAS ENCORE BOUFFÉ, LA PÉNALITÉ EST A 20% DE TA COTISATION**
>>>>
>>>
>> **6. COLLATION ET DIVERS**
>>> 
